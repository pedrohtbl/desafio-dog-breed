import React, { createContext, useContext, useState, ReactNode } from "react";

interface IProps {
    children: ReactNode;
}

interface IUserContext {
    token: string;
    setToken : React.Dispatch<React.SetStateAction<string>>;
    isLoading: boolean;
    setIsLoading : React.Dispatch<React.SetStateAction<boolean>>;
}

const DEFAULT_VALUE = {
    token: '',
    setToken: () => {},
    isLoading: true,
    setIsLoading: () => {}
}

export const TokenContext = createContext<IUserContext>(DEFAULT_VALUE)

export const TokenProvider = ({children}: IProps) =>{
    const [token, setToken] = useState<string>( sessionStorage.getItem('token') || DEFAULT_VALUE.token)
    const [isLoading, setIsLoading] = useState<boolean>(DEFAULT_VALUE.isLoading)

    return(
        <TokenContext.Provider value={{token, setToken, isLoading, setIsLoading}}>
            {children}
        </TokenContext.Provider>
    )
}

export const useToken = () => useContext(TokenContext)