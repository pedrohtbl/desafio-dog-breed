import { ReactNode } from "react"
import { TokenProvider } from "./token"

interface IProps {
    children: ReactNode
}

const Provider = ({children}: IProps) =>{
    return(
        <TokenProvider>
            {children}
        </TokenProvider>
    )
}

export default Provider