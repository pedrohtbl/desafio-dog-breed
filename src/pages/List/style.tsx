import { Modal } from "@mui/material";
import styled from "styled-components";

export const CustomMain = styled.main`
    display: flex;
    flex-direction: column;
    min-height: 100vh;
    min-width: 100%;
    align-items: center;
    padding-top: 50px;
    background-color: var(--primary-color);
`

export const CustomModal = styled(Modal)`
    display: flex;
    justify-content: center;
    align-items: center;
    
    img{
        max-width: 70vw;
        max-height: 80vh;
    }

`

export const CustomUl = styled.ul`
    display: flex;
    flex-wrap: wrap;
    width: 75vw;
    justify-content: space-evenly;
    margin-top: 50px;

    >div{
        min-height: 50vh;
        display: flex;
        align-items: center;
    }

    li{
        margin-bottom: 20px;
        background-color: var(--secundary-color);
        width: 250px;
        min-width: 250px;
        height: 300px;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;
    }

    img{
        width: 200px;
        height: 250px;
    }
`