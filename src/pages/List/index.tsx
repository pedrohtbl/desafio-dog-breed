import { ButtonGroup, Button, CircularProgress } from "@mui/material"
import { useEffect, useState } from "react"
import { Redirect } from "react-router-dom"
import { useToken } from "../../provider/token"
import { api } from "../../service"
import { CustomMain, CustomModal, CustomUl } from "./style"

const List = () =>{
    const {token,isLoading, setIsLoading} = useToken()
    const [breed, setBreed] = useState<string>("")
    const [list, setList] = useState<[]>([])
    const [openModal, setOpenModal] = useState<boolean>(false)
    const [imgModal, setImgModal] = useState<string>("")
    
    useEffect(()=>{
        api.get(`/list?breed=${breed}`,{
            headers:{
                Authorization: token
            }
        })
        .then(res => {
            setTimeout(()=>{
                setList(res.data.list)
                setIsLoading(false)
            },1500)
        })
    },[isLoading])

    const changeBreed = (newBreed: string): void =>{
        setBreed(newBreed)
        setIsLoading(true)
    }

    const handleClose = (): void =>{
        setOpenModal(false)
    }    

    const handleOpen = (item: string): void =>{
        setOpenModal(true)
        setImgModal(item)
    }
    
    if(!token){
        return <Redirect to={"/"}/>
    }

    return(
        <>
        <CustomMain>
            <CustomModal
                open={openModal}
                onClose={handleClose}
            >
                <img src={imgModal}/>
            </CustomModal>

            <ButtonGroup color="secondary" size="small">
                <Button onClick={()=> changeBreed('chihuahua')}>Chihuahua</Button>
                <Button onClick={()=> changeBreed('husky')}>Husky</Button>
                <Button onClick={()=> changeBreed('labrador')}>Labrador</Button>
                <Button onClick={()=> changeBreed('pug')}>Pug</Button>
            </ButtonGroup>

            <CustomUl>
                {isLoading? 
                    <div>
                        <CircularProgress color="secondary" size={100}/>
                    </div>
                
                :
                
                    list.map((item,index) =>
                        <li key={index} onClick={()=> handleOpen(item)}>
                            <img src={item}/>
                        </li>
                    )}

            </CustomUl>
        </CustomMain>
        </>
    )
}

export default List