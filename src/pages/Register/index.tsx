import { Button, TextField } from "@mui/material"
import { Form, Main } from "./style"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import { useForm } from "react-hook-form"
import { api } from "../../service"
import { toast } from "react-toastify"
import { useHistory } from "react-router-dom"
import { useToken } from "../../provider/token"

interface IFormData  {
    email: string
}

const Register = () =>{
    const schema = yup.object().shape({
        email: yup.string().required("Required Field").email("Invalid Email")
    })

    const {register, handleSubmit, formState: {errors} } = useForm<IFormData>({
        resolver: yupResolver(schema)
    })

    const history = useHistory()

    const {token, setToken} = useToken()

    const onSubmit = (data: IFormData) =>{
        api.post("/register", data)
        .then(res =>{
            toast.success("Success login",{
                theme: "dark"
            })
            sessionStorage.setItem("token", res.data.user.token)
            setToken(res.data.user.token)
            history.push("/list")
        })
        .catch(err =>{
            toast.error("Wrong email",{
                theme: "dark"
            })
        })
    }

    return(
        <Main>
            <h1>Register</h1>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <TextField variant="outlined" label="Email" {...register("email")}/>
                    {errors && <p>{errors.email?.message}</p>}
                </div>
                <Button variant="contained" type="submit">Register</Button>
            </Form>
        </Main>
    )
}

export default Register