import styled from "styled-components";

export const Main = styled.main`
    min-height: 80vh;
    display: flex;
    flex-direction: column;
    justify-content: center;

    h1{
        width: 100%;
        text-align: center;
        color: var(--primary-color);
    }
`

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    min-height: 30vh;
    max-width: 300px;
    width: 100vw;

    .MuiButton-root{
        background-color: var(--secundary-color);
    }

    .MuiButton-root:hover{
        background-color: var(--violet-color);
    }

    div{
        width: 100%;
    }

    p{
        color: var(--error-color);
        font-weight: 500;
        width: 100%;
        margin-top: 10px;
    }

`