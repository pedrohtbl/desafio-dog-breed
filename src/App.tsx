import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Routes from './routes';
import GlobalStyle from './style'

function App() {
    return (
    <>
        <ToastContainer
        	position="bottom-center"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss={false}
            draggable
            pauseOnHover={false}
        />
        <Routes/>
        <GlobalStyle/>
    </>
    );
}

export default App;
