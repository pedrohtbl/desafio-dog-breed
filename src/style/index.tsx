import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;
        font-family: sans-serif;
    }

    :root{
        --primary-color: #03030363;
        --secundary-color: #9e41f5;
        --violet-color: #602993;
        --white-color: #FFFFFF;
        --error-color: #ee3f3f;
    }
    
    #root{
        display: flex;
        flex-direction: column;
        align-items: center;
        min-height: 100vh;
        color: 	var(--primary-color);
    }

    a{
        text-decoration: none;
    }

    button{
        cursor: pointer;
    }

    ul{
        list-style: none;
    }
`