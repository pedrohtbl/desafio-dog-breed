import { Route, Switch } from "react-router-dom"
import List from "../pages/List"
import Register from "../pages/Register"

const Routes = () =>{
    return(
        <Switch>
            <Route exact path={'/'}>
                <Register/>
            </Route>
            <Route path={'/list'}>
                <List/>
            </Route>
        </Switch>
    )
}

export default Routes